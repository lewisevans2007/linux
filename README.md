# Linux kernel

This is my modified version of the linux kernel. This repository is just a experimental clone and might contain some weird changes, comments and code. The main purpose of this repository is to learn more about the linux kernel and tinker around with it.

This repo is based on the latest stable version of the linux kernel (6.6.12) as of 2024-1-17. See the main Linux kernel repository [here](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree).
